/**
 * Workie - a tool to help focus in the face of procrastination
 * 
 * Contributors:
 *  Aaron S. Crandall <acrndal@gmail.com>, 2019
 * 
 * Version: v0.1-PreAlpha
 * Codename: "Juli needs to get her applications in!"
 * 
 */

#define VIBE_PIN 0
#define SPEAKER_PIN 1
#define TIMEOUT_LENGTH_PIN 2
#define WORK_BUTTON_PIN 3
#define WORK_LED_PIN 4

#define FIVE_MINS_MS    1000 * 60 * 5
#define TWENTY_MINS_MS  1000 * 60 * 20

#define BUTTON_PRESSED LOW
#define BUTTON_RELEASED HIGH

enum STATES {START, IDLE, WORKING};

int g_work_timeout_ms = FIVE_MINS_MS;
int g_last_work_button_state = BUTTON_RELEASED;


// ***************************************************** //
void setup() {
  // Configure pins for in/out
  pinMode(VIBE_PIN, OUTPUT);
  pinMode(SPEAKER_PIN, OUTPUT);
  pinMode(TIMEOUT_LENGTH_PIN, INPUT_PULLUP);
  pinMode(WORK_BUTTON_PIN, INPUT_PULLUP);
  pinMode(WORK_LED_PIN, OUTPUT);

  // Default pin states
  digitalWrite(VIBE_PIN, LOW);
  digitalWrite(WORK_LED_PIN, LOW);


}



// Read the timeout switch for work time length
void get_set_work_timeout() {
  if( digitalRead(TIMEOUT_LENGTH_PIN) == HIGH ) {
    g_work_timeout_ms = FIVE_MINS_MS;
  } else {
    g_work_timeout_ms = TWENTY_MINS_MS;
  }
}


// ****************** MUSIC SECTION **************** //
void play_tone() {
  tone(SPEAKER_PIN, 500, 1000);
}

// ****************** Motor SECTION **************** //
void vibrate(int ms) {
  digitalWrite(VIBE_PIN, HIGH);
  delay(ms);
  digitalWrite(VIBE_PIN, LOW);
}


// ****************** Button/Lights SECTION **************** //
void turn_on_work_light() {
  digitalWrite(WORK_LED_PIN, HIGH);
}
void turn_off_work_light() {
  digitalWrite(WORK_LED_PIN, LOW);
}
void blink_work_light(int ms) {
  turn_on_work_light();
  delay(ms);
  turn_off_work_light();
}

bool is_work_button_pressed() {
  return digitalRead(WORK_BUTTON_PIN) == LOW;
}

bool was_work_button_pressed() {
  if( is_work_button_pressed() && g_last_work_button_state == BUTTON_RELEASED ) {
    return true;
  }
  return false;
}

bool was_work_button_released() {
  if( !is_work_button_pressed() && g_last_work_button_state == BUTTON_PRESSED ) {
    return true;
  }
  return false;
}


// ***************************************************** //
void loop() {
  get_set_work_timeout();             // Determine current work session timeout



  if( g_work_timeout_ms == FIVE_MINS_MS ) {
    play_tone();
  } else {
    
  }

  if( was_work_button_pressed() ) {
    g_last_work_button_state = BUTTON_PRESSED;
    blink_work_light(250);
    vibrate(100);
  }

  if( was_work_button_released() ) {
    g_last_work_button_state = BUTTON_RELEASED;
    blink_work_light(100);
  }
  
  delay(2000);
  
}
