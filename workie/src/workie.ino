/**
 * Workie - a tool to help focus in the face of procrastination
 * 
 * Contributors:
 *  Aaron S. Crandall <acrndal@gmail.com>, 2019
 * 
 * Version: v0.1-PreAlpha
 * Codename: "Juli needs to get her applications in!"
 * 
 */


// #include "Arduino.h"    // Let PlatformIO know the defined builtins

#define VIBE_PIN 0
#define SPEAKER_PIN 1
#define TIMEOUT_LENGTH_PIN 2
#define WORK_BUTTON_PIN 3
#define WORK_LED_PIN 4

//#define FIVE_MINS_MS     300000    // 5 mins
#define FIVE_MINS_MS      10000    // 10 sec mins (testing)
#define TWENTY_MINS_MS  1200000   // 20 mins
#define CANCEL_TIMEOUT_MS 5000    // 5 seconds

#define BUTTON_PRESSED LOW
#define BUTTON_RELEASED HIGH

long g_work_timeout_ms = FIVE_MINS_MS;

int g_last_work_button_state = BUTTON_RELEASED;
bool g_work_button_state_changed = false;

enum state {START, IDLE, WORKING};
state g_curr_state = START;

unsigned long g_working_start_millis = 0;
unsigned long g_button_pressed_start_millis = 0;

// TinyTone for ATtiny85 //
// Notes
const int Note_C  = 239;
const int Note_CS = 225;
const int Note_D  = 213;
const int Note_DS = 201;
const int Note_E  = 190;
const int Note_F  = 179;
const int Note_FS = 169;
const int Note_G  = 159;
const int Note_GS = 150;
const int Note_A  = 142;
const int Note_AS = 134;
const int Note_B  = 127;



// ***************************************************** //
void setup() {
  g_curr_state = START;

  // Configure pins for in/out
  pinMode(VIBE_PIN, OUTPUT);
  pinMode(SPEAKER_PIN, OUTPUT);
  pinMode(TIMEOUT_LENGTH_PIN, INPUT_PULLUP);
  pinMode(WORK_BUTTON_PIN, INPUT_PULLUP);
  pinMode(WORK_LED_PIN, OUTPUT);

  // Default pin states
  digitalWrite(VIBE_PIN, LOW);
  digitalWrite(WORK_LED_PIN, LOW);

  play_scale();

  g_curr_state = IDLE;
}



// Read the timeout switch for work time length
void get_set_work_timeout() {
  if( digitalRead(TIMEOUT_LENGTH_PIN) == LOW ) {
    g_work_timeout_ms = FIVE_MINS_MS;
  } else {
    g_work_timeout_ms = TWENTY_MINS_MS;
  }
}


// ****************** MUSIC SECTION **************** //
void play_tone() {
  TinyTone(Note_C, 2, 500);
}

void play_success() {
  
}

void play_clicker_sound() {
  TinyTone(Note_E, 4, 200);
  delay(100);
  TinyTone(Note_C, 5, 500);
}

void TinyTone(unsigned char divisor, unsigned char octave, unsigned long duration)
{
  //TCCR1 = 0x90 | (8-octave); // for 1MHz clock
  TCCR1 = 0x90 | (11-octave); // for 8MHz clock
  OCR1C = divisor-1;         // set the OCR
  delay(duration);
  TCCR1 = 0x90;              // stop the counter
  
  digitalWrite(SPEAKER_PIN, LOW);    // Single high pitched PWM pin whine?
}

// Play a scale
void play_scale(void)
{
 TinyTone(Note_C, 4, 500);
 TinyTone(Note_D, 4, 500);
 TinyTone(Note_E, 4, 500);
 TinyTone(Note_F, 4, 500);
 TinyTone(Note_G, 4, 500);
 TinyTone(Note_A, 4, 500);
 TinyTone(Note_B, 4, 500);
 TinyTone(Note_C, 5, 500);
}

// ****************** Motor SECTION **************** //
void vibrate(int ms) {
  digitalWrite(VIBE_PIN, HIGH);
  delay(ms);
  digitalWrite(VIBE_PIN, LOW);
}

// ****************** Button/Lights SECTION **************** //
void turn_on_work_light() {
  digitalWrite(WORK_LED_PIN, HIGH);
}
void turn_off_work_light() {
  digitalWrite(WORK_LED_PIN, LOW);
}
void blink_work_light(int ms) {
  turn_on_work_light();
  delay(ms);
  turn_off_work_light();
}

bool check_is_work_button_pressed() {
  return digitalRead(WORK_BUTTON_PIN) == LOW;
}

bool check_was_work_button_pressed() {
  if( check_is_work_button_pressed() && g_last_work_button_state == BUTTON_RELEASED ) {
    return true;
  }
  return false;
}

bool check_was_work_button_released() {
  if( !check_is_work_button_pressed() && g_last_work_button_state == BUTTON_PRESSED ) {
    return true;
  }
  return false;
}

void get_set_button_status() {
  if( check_was_work_button_pressed() ) {
    g_work_button_state_changed = true;
    g_last_work_button_state = BUTTON_PRESSED;
  } else if( check_was_work_button_released() ) {
    g_work_button_state_changed = true;
    g_last_work_button_state = BUTTON_RELEASED;
  } else {
    g_work_button_state_changed = false;
  }
}

bool is_work_button_pressed() {
  return g_last_work_button_state == BUTTON_PRESSED;
}
bool was_work_button_pressed() {
  return( g_work_button_state_changed == true && g_last_work_button_state == BUTTON_PRESSED );
}
bool was_work_button_released() {
  return( g_work_button_state_changed == true && g_last_work_button_state == BUTTON_RELEASED );
}

// ****************** Behavior SECTION **************** //
bool am_idle()    { return g_curr_state == IDLE; }
bool am_working() { return g_curr_state == WORKING; }

void do_idle_state() {
  if( was_work_button_pressed() ) {
    enter_working_state();
  }
}

void enter_working_state() {
  g_working_start_millis = millis();
  g_curr_state = WORKING;

  vibrate(250);
  turn_on_work_light();

  while( check_is_work_button_pressed() ) {
    // Wait until button released -- issues in working state & clicker
  }
}

void leave_working_state() {
  g_curr_state = IDLE;

  vibrate(250);
  delay(100);
  vibrate(250);
  play_tone();    // TODO: fix for a music score
  for(int i = 0; i < 5; i++) {
    blink_work_light(500);
    delay(500);
  }
  turn_off_work_light();
}

void cancel_working_state() {
  g_curr_state = IDLE;
  // Should play a sad tune here
  turn_off_work_light();
}

bool is_work_done() {
  return (millis() >= g_working_start_millis + g_work_timeout_ms);
}

bool is_work_cancelled() {
  return(is_work_button_pressed() && millis() > g_button_pressed_start_millis + CANCEL_TIMEOUT_MS );
}

bool is_clicker() {
  if( was_work_button_released() ) {
    return true;
  }
  return false;
}

void play_clicker_reward() {
  turn_off_work_light();
  play_clicker_sound();
  turn_on_work_light();
}

void do_working_state() {
  unsigned long curr_millis = millis();

  if( is_work_done() ) {                    // Work done!
    leave_working_state();       
  } else {                                  // Check cancel & clicker
    if( was_work_button_pressed() ) {
      g_button_pressed_start_millis = millis();
    } else if( is_work_cancelled() ) {      // cancel button pressed long enough to cancel work
      cancel_working_state();
    } else if( is_clicker() ) {
      play_clicker_reward();
    }
  }
}

// ***************************************************** //
void loop() {
  get_set_work_timeout();           // Determine current work session timeout
  get_set_button_status();          // Determine control button status

  if( am_idle() ) {
    do_idle_state();
  } else if( am_working() ) {
    do_working_state();
  }

  delay(100);                       // Let things rest a bit
}
